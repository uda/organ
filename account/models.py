from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Look at this: https://docs.djangoproject.com/en/1.10/topics/auth/customizing/


class MemberType(models.Model):
    name = models.CharField(max_length=32)

    DEFAULT_PK = 1

    def __str__(self):
        return self.name


class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    type = models.ForeignKey(MemberType, default=MemberType.DEFAULT_PK)
    joined = models.DateField()
    renewed = models.DateField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.get_full_name() or self.user.get_username()
