# Organ - NPO toolkit

Organ provides NPO's with a toolkit to manage basic membership.

## Dependencies

Some dependencies are explained here:

### Python

* Django (Well...)
* python-social-auth - provides access via social apps
* social-auth-app-django - Provides a django bridge
* django-avatar - Avatar management and Gravatar default
* Twisted - As an HTTP server
* pipenv - A really better pip dependency manager
* django-bower - Connects django with bower package manager

### Frontend

* AdminLTE - A bootstrap based theme
* Bootstrap
* jQuery
* FontAwesome - Awesome icon font
* Ionicons - Some more icons

## License

Main project "Organ" is under the MIT license (Expat license).

Dependencies' licenses may vary, most use MIT, but that might not always be the case.
